import Vue from 'vue'
import Router from 'vue-router'
import Main from '../components/main'
import Accessory from '../components/accessory'
import Uslugi from '../components/uslugi'
import States from '../components/states'
import Contacts from '../components/contacts'
Vue.use(Router)
const routes=[
    {
        path:'/',
        name:'main',
        component:Main
    },
    {
        path:'accessory',
        name:'accessory',
        component:Accessory
    }
    ,
    {
        path:'uslugi',
        name:'uslugi',
        component:Uslugi
    },
    {
        path:'contacts',
        name:'contacts',
        component:Contacts
    },
    {
        path:'states',
        name:'states',
        component:States
    }

]
const router = new Router({
    mode: 'history',
    routes: routes
});
export default router;